# Homework completed while taking the Python starter course. 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
## Course provided by https://edu.cbsystematics.com :thumbsup:
   Thank's to the team. Very polite staff and quite understandable
   and simple material was presented for me. :wink:
   
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
## Studied material:
###  1. Introduction to python. 
    On this lesson I got an idea of the peculiarities of the language
    Python programming, history of its appearance, advantages and disadvantages, areas
    applications, learn to use the interpreter and create my first program on this
    language.
### 2. Variables and data types.
    On this lesson, I learned how to store values in variables and work with them.
    - Calculate the values of arithmetic and logical expressions in their programs
    use formatted output.
### 3. Conditional statements.
    - On this lesson, I learned how to compose algorithms with branches.
    - Use conditional statements in Python.
### 4. Loop statements.
    On this lesson discusses looping constructs.
    After studying the material of this lesson, I learned:
    - Make loop algorithms.
    - Use the while and for, break and continue statements.
### 5. Functions_1.
    On this lesson, we covered functions.
    After studying the material of this lesson, I learned:
    - Understand what functions are and how i can use them.
    - Create my own functions.
    - Use named arguments when calling functions, set arguments in random order.
    - Use optional parameters.
### 6. Functions_2.
    On this lesson discusses more advanced use of functions, discusses the concept
    recursion, provides an overview of some of the standard Python functions.
    After studying the material of this lesson, i learned:
    - Create a special kind of comments - document lines.
    - Use standard Python functions.
    - Understand the difference between local and global variables.
    - Use recursion.
### 7. Lists.
    On this lesson discusses lists - a data structure that allows me to store multiple
    values and in Python is often used instead of arrays in other programming languages.
    After studying the material of this lesson, i learned:
    - Use lists to store and process multiple values.
    - Receive individual elements of lists and lines, as well as their sections
    work with lists: add and remove data, change existing values,
    check items for entry into the list.
    - Understand the difference between mutable and immutable data structures.
 ### 8. Specification PEP 8.
    Gain knowledge of the PEP 8 specification and acquire the skills to write simple and readable code.
    After studying the material of this lesson, i learned:
    - Understand the main goals and reasons for creating PEP specifications.
    - Write simpler, more comprehensible code based on the best practices of this specifications.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# It was a great experience for me and a great time. :muscle: :fire:
