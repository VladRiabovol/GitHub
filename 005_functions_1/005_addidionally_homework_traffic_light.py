# Program of working traffic light for pedestrian and driver.


# Init the program
def main():

    # Input mode
    time = enter_time()
    mode = choose_mode()

    # Mode of working traffic light
    light = 'red'
    while light == 'red' or light == 'yellow':
        time -= 1
        light = traffic_light(mode, time)


# Entering time of working traffic light
def enter_time():
    check_time = int(input('Please enter time of lightning, from 10 till 15: '))
    while True:
        if 9 < check_time < 16:
            return check_time
        else:
            print('Error entry, please enter from 10 till 15')
            check_time = int(input('Please enter time of lightning: '))


# Choosing mode of traffic light.
def choose_mode():
    while True:
        input_mode = input("Choose mode of traffic light: 'pedestrian' or 'driver' ")
        if input_mode == 'pedestrian' or input_mode == 'driver':
            return input_mode
        else:
            print('Error, please choose \'pedestrian\' or \'driver\' ')
            input_mode = input("Choose mode of traffic light: 'pedestrian' or 'driver' ")


# Choosing mode for pedestrian or driver
def traffic_light(order, wait):
    if order == 'driver':
        return driver(wait)
    else:
        return pedestrian(wait)

# Mode working of traffic light for driver
def  driver(wait):
    if wait > 9:
        print('You need to wait!')
        return 'red'
    elif wait == 10:
        print('Get ready!')
        return 'yellow'
    else:
        print('Now you can ride!')
        return 'green'

# Mode working of traffic light for pedestrian
def pedestrian(wait):
    if wait:
        print('You need to wait!')
        return 'red'
    else:
        print('Now you can go!')
        return 'green'

if __name__ == '__main__':
    main()