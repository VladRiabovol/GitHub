# Creates a list containing only words starting with the letter "c".


def sentence_in_word_c(sentences):
    list_with_c = []
    list_of_words = []
    for sentence in sentences:
        temp = sentence.lower().split(' ')
        list_of_words.append(temp)
        for word in temp:
            if word.startswith('с'):
                list_with_c.append(word)
    return list_with_c


def main():
    list_of_strings = ["Список - изменяемый тип данных", "Строка - неизменяемый тип данных",
                       "Строковый метод работает быстрее, чем срез",
                       "Для обхода последовательности используйте совместный цикл"]
    words_start_c = sentence_in_word_c(list_of_strings)
    print(words_start_c)

if __name__ == "__main__":
    main()