import math

x = int(input("Please enter value х: "))
P = math.pi

if -P <= x <= P:
    y = math.cos(3 * x)
    print("Y = {}".format(y))
elif x < -P or x > P:
    y = x
    print('Y = {}'.format(y))

