x = float(input('Enter a number: '))
check = None
check = "Integer number" if x.is_integer() is True else "Fractional number, round to the nearest integer"
print(check)
x = round(x) if x.is_integer() is False else int(x)

if x % 2 == 0 and x < 0:
    print(x, "is an even negative number.")
elif x % 2 == 0 and x > 0:
    print(x, "is an even positive number.")
elif x % 2 != 0 and x < 0:
    print(x, "is an odd negative number.")
elif x % 2 != 0 and x > 0:
    print(x, "is an odd positive number.")
