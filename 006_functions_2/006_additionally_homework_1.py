

def recurs(end):
    if end == 1:
        return 1
    else:
        return end + recurs(end - 1)

input_range = int(input("Input range from 0 to: "))


x = recurs(input_range)
print('Sum of natural numbers from 0 to {} is {}.'.format(input_range, x))