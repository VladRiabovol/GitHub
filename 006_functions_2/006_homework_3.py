
# Определяю функцию которая принимает определенную ступеньку и вычисляет
# колличество способов которыми можно поднятся на нее
def count_of_ways(quantity):
    if quantity == 1 or quantity == 0:
        return 1
    else:
        return count_of_ways(quantity - 1) + count_of_ways(quantity - 2)


# Определяю функцию которая запрашивает номер ступеньки и возвращает
# колличество способов которыми можно подняться на нее

def main():
    request = int(input("Введите ступеньку: "))
    result = count_of_ways(request)
    return 'Колличество способов = {}'.format(result)


x = main()
print(x)





