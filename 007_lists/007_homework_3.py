

# Ask range of search and add simple numbers from range to list.
def enter_range():
    index_1 = int(input('Input start range: '))
    index_2 = int(input('Input final range: '))
    my_list = list(range(index_1, index_2 + 1))
    for i in my_list:
        if i < 2:
            continue
        if search_natural(i):
            natural.append(i)
    print("Numbers which find from entered range:", natural)


# Search simple numbers.
def search_natural(number):
    for x in range(2, number):
        if number % x == 0:
            return False
    return True


# Ask for operation what to do with finds simple numbers.
def request():
    operation = input('''Choose what you wan't to do with natural numbers what was find:
                        1.Multiply
                        2.Addition
                        else: Exit program
                        ''')
    while True:
        if operation == "2":
            x = sum(natural)
            print('Addition of find numbers = {}'.format(x))
            return
        elif operation == "1":
            multiply = natural[0]
            for i in natural[1:]:
                multiply *= i
            print('Multiply of find numbers = {}'.format(multiply))
            return
        else:
            print("Exit")
            return

natural = []

# Runs the program.
def main():
    enter_range()
    request()

if __name__ == '__main__':
    main()