

# Returns max value.
def max_value(value):
    max = []
    max = my_list[0]
    for element in my_list:
        if element > max:
            max = element
    return max

# Returns min value.
def min_value(value):
    min = []
    min = my_list[0]
    for element in my_list:
        if element < min:
            min = element
    return min


# Returns the sum of elements from an actual parameter.
def sum_value(value):
    sum_elements = []
    sum_elements = sum(my_list)
    return sum_elements


# Returns the average of an actual parameter.
def average_value(value):
    average = max_value(my_list) / len(my_list)
    return average


my_list = [1, 16, 18, 22, 33, 4, 18, 100, 333, 14]

print("Max element is {}".format(max_value(my_list)))
print("Min element is {}".format(min_value(my_list)))
print("Sum of elements is {}".format(sum_value(my_list)))
print("Average of elements is {}".format(average_value(my_list)))