import math

x = float(input("Please enter 1 value: "))
y = float(input("Please enter 2 value: "))
operator = input('Please enter operation: ')

if x is None or y is None:
    print('Entered value can"t be empty' )
else:
    if operator == '+':
        print(x + y)
    elif operator == '-':
        print(x - y)
    elif operator == '*':
        print(x * y)
    elif operator == '/':
        print(x / y)
    elif operator == '**':
        print(x ** y)
    elif operator == 'sin':
        print(math.sin(x), math.sin(y), sep = '; ')
    elif operator == 'cos':
        print(math.cos(x), math.cos(y), sep = '; ')
    elif operator == 'tan':
        print(math.tan(x), math.tan(y), sep = '; ')
