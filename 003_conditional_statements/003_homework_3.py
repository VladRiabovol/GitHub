a = int(input('Please enter value а: '))
b = int(input('Please enter value b: '))
c = int(input('Please enter value c: '))
D = b ** 2 - 4 * a * c

if D < 0:
    print('The D value is negative, therefore there are no roots')
elif D == 0:
    x = -(b / (2 * a))
    print('Value D is zero, therefore x = {}'.format(x))
elif D > 0:
    x1 = (-b - D ** 0.5) / (2 * a)
    x2 = (-b + D ** 0.5) / (2 * a)
    print('The value of D is positive, therefore x1 = {}, x2 = {}'.format(x1, x2))
