# Count the number of ways in which you can climb a step


def count_of_ways(quantity):
    ways = [1, 1]
    for step in range(quantity - 2):
        ways.append(ways[step] + ways[step + 1])

    quantity = (ways[-1] + ways[-2])
    return quantity


def main():
    request = int(input("Enter a step: "))
    result = count_of_ways(request)
    print('Count of ways is {}.'.format(result))


if __name__ == '__main__':
    main()





