# Calculator.


def addition(x, y):
    return x + y

def subtraction(x, y):
    return x - y


def multiplication(x, y):
    return x * y


def division(x, y):
    return x / y


while True:
    check = int(input('''
    Enter 1 to use the calculator.
    Enter 2 to exit the program.
    '''))
    if check == 1:
        x = float(input("Please enter 1 value: "))
        y = float(input("Please enter 2 value: "))
        operator = input('Please enter one of the specified operations: +, -, /, * ')
        if operator == '+':
            operation = addition(x, y)
            print(operation)
        elif operator == '-':
            operation = subtraction(x, y)
            print(operation)
        elif operator == '*':
            operation = multiplication(x, y)
            print(operation)
        elif operator == "/":
            if x and y:
                operation = division(x, y)
                print(operation)
            else:
                print('An input error was made, the operation / cannot use the value 0')
    else:
        print('The program has ended.')
        break