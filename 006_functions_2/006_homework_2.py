

# Removes spaces from an expression.
def cut_spaces(word):
    z = ''
    for i in word:
        if i == ' ':
            continue
        z += i
    return z


# Reverses the entered phrase and removes spaces.
def reverse(word):
    y = ''
    for i in reversed(word):
        if i == ' ':
            continue
        y += i
    return y


# Checks if the entered phrase is a palindrome.
def is_palindrome(variable):
    if cut_spaces(variable) == reverse(variable):
        print("Palindrome")
        return "Palindrome"
    else:
        print('No Palindrome')
        return "No Palindrome"

# Starts the programm
def mode():
    enter_variable = input("Please enter variable for check palindrome or no palindrome: ")
    variable_for_check = is_palindrome(enter_variable)


if __name__ == "__main__":
    mode()